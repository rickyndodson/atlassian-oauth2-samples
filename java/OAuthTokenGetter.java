import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A standalone program demonstrating how to obtain and use a token from the authorization service.
 * Not intended for use in production due to hand-rolled JWT / JSON implementations
 * (see https://jwt.io/#libraries for JWT library suggestions).
 * <p>
 * Your add-on (and the token) will need the ACT_AS_USER scope, and you will need to be able to access the install data from
 * your add-on service in order to get the required argument values.
 *
 * Note that the useraccountid is not the same as the username - see the usage message for more info.
 * <p>
 * The OAuth client ID is included in the install hook payload as <tt>oauthClientId</tt>.
 * <p>
 * Requirements: JDK 8
 * To run (in the same directory as this file):
 * <pre>
 *  javac OAuthTokenGetter.java
 *  java OAuthTokenGetter {oauth-client-id} {instance-base-url} {useraccountid-to-act-as} {secret}
 * </pre>
 */
public class OAuthTokenGetter {

    public static final String AUTHORIZATION_SERVER_URL = "https://oauth-2-authorization-server.services.atlassian.com";
    public static final long EXPIRY_SECONDS = 10;
    public static final String GRANT_TYPE = "urn:ietf:params:oauth:grant-type:jwt-bearer";
    public static final String SCOPES = "READ ACT_AS_USER"; // case-sensitive space-delimited as per the specification
                                                // (https://tools.ietf.org/html/rfc6749#section-3.3)

    public static final String USAGE_MESSAGE =
            "Usage: java OAuthTokenGetter {oauth-client-id} {instance-base-url} {useraccountid-to-act-as} {secret}"
                    + "\n\nPLEASE NOTE: useraccountid is not the same thing as username (the former never changes for a given user)."
                    + "\nTo get the key of user `alex`, use the REST endpoint /rest/api/2/user?username=alex in JIRA or "
                    + "/rest/api/user?username=alex in Confluence.";

    public static final Charset UTF8 = Charset.forName("UTF-8");

    public static void main(String[] args) throws Exception {
        if (args.length != 4) {
            System.err.println(USAGE_MESSAGE);
            System.exit(2);
        }
        final String userAccountId = args[2];
        final String instanceBaseUrl = args[1].replaceAll("/+$", ""); // Strip any trailing slashes
        final String clientId = args[0];
        final String secret = args[3];

        print("\nBUILDING JWT ASSERTION:");
        String assertion = createAssertion(instanceBaseUrl, userAccountId, clientId, secret);
        print("\nREQUESTING ACCESS TOKEN:");
        Map<String, String> parameters = new HashMap<>();
        parameters.put("grant_type", GRANT_TYPE);
        parameters.put("assertion", assertion);
        parameters.put("scope", SCOPES);
        String accessTokenResponseBody = post(AUTHORIZATION_SERVER_URL + "/oauth2/token", parameters);
        print("got access token response:");
        print(accessTokenResponseBody);

        String accessToken = getAccessTokenFromResponseBody(accessTokenResponseBody);

        String testResource = instanceBaseUrl.endsWith("/wiki") ? "/rest/api/user/current" : "/rest/api/latest/myself";
        String fullResourcePath = instanceBaseUrl + testResource;
        print("\nMAKING REQUEST AS USER to " + fullResourcePath + " as " + userAccountId);
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + accessToken);
        headers.put("Accept", "application/json");
        String requestAsUserResponse = get(fullResourcePath, headers);

        print("Got response:");
        print(requestAsUserResponse);
    }

    // Do not try this at home (or in production). Use a JSON library (see the list at http://json.org/).
    private static String getAccessTokenFromResponseBody(String accessTokenResponseBody) {
        Pattern tokenFinder = Pattern.compile("\"access_token\" *: *\"([^\"]+)\"");
        Matcher matcher = tokenFinder.matcher(accessTokenResponseBody);
        if (!matcher.find()) {
            System.err.println("Couldn't get access token from response");
            System.exit(4);
        }
        return matcher.group(1);
    }

    // Do not try this at home (or in production). Use a JWT library (see https://jwt.io/#libraries).
    private static String createAssertion(String instanceBaseUrl,
                                          String userAccountId,
                                          String clientId,
                                          String secret)
            throws InvalidKeyException, NoSuchAlgorithmException {
        long now = System.currentTimeMillis() / 1000;
        long exp = now + EXPIRY_SECONDS;
        byte[] header = "{ `alg`: `HS256`, `typ`: `JWT` }".replace("`", "\"").getBytes(UTF8);
        byte[] payload = Arrays.asList(
                "{   `iss`: `urn:atlassian:connect:clientid:" + clientId + "`,",
                "    `sub`: `urn:atlassian:connect:useraccountid:" + userAccountId + "`,",
                "    `tnt`: `" + instanceBaseUrl + "`,",
                "    `aud`: `" + AUTHORIZATION_SERVER_URL + "`,",
                "    `iat`: " + now + ",",
                "    `exp`: " + exp + "    }"
        ).stream().collect(Collectors.joining(System.lineSeparator())).replace("`", "\"").getBytes(UTF8);
        print("assertion payload:");
        print(new String(payload, UTF8));
        Base64.Encoder encoder = Base64.getUrlEncoder();
        String headerAndPayloadSegments = String.format("%s.%s",
                new String(encoder.encode(header), UTF8), new String(encoder.encode(payload), UTF8));
        byte[] signature = sign(headerAndPayloadSegments.getBytes(UTF8), secret);
        String jwt = String.format("%s.%s",
                headerAndPayloadSegments, new String(encoder.encode(signature), UTF8));
        print("assertion JWT: " + jwt);
        return jwt;
    }

    private static byte[] sign(byte[] payload, String secret) throws NoSuchAlgorithmException, InvalidKeyException {
        String Hs256 = "HmacSHA256";
        Mac signer = Mac.getInstance(Hs256);
        signer.init(new SecretKeySpec(secret.getBytes(), Hs256));
        return signer.doFinal(payload);
    }

    private static String get(String url, Map<String, String> headers) {
        try {
            return makeRequest(url, headers, new HashMap<>());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String post(String url, Map<String, String> formParameters) {
        try {
            return makeRequest(url, new HashMap<>(), formParameters);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String makeRequest(String url, Map<String, String> headers, Map<String, String> formParameters)
            throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        for (Map.Entry<String, String> header : headers.entrySet()) {
            connection.setRequestProperty(header.getKey(), header.getValue());
        }
        String method = formParameters.isEmpty() ? "GET" : "POST";
        print(method + " to " + url);
        connection.setRequestMethod(method);
        if (method.equals("POST")) {
            connection.setDoOutput(true);
            connection.setRequestProperty("Accept-Charset", UTF8.name());
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + UTF8.name());
            StringBuilder formBodyBuilder = new StringBuilder();
            Boolean firstParam = true;
            for (Map.Entry<String, String> param : formParameters.entrySet()) {
                if (!firstParam) {
                    formBodyBuilder.append("&");
                } else {
                    firstParam = false;
                }
                formBodyBuilder.append(URLEncoder.encode(param.getKey(), UTF8.name()))
                        .append("=")
                        .append(URLEncoder.encode(param.getValue(), UTF8.name()));
            }
            final String formBodyString = formBodyBuilder.toString();
            print("form body:");
            print(formBodyString);
            byte[] formBody = formBodyString.getBytes(UTF8);
            connection.setRequestProperty("Content-Length", Integer.toString(formBody.length));
            new DataOutputStream(connection.getOutputStream()).write(formBody);
        }
        int responseCode = connection.getResponseCode();
        if (responseCode >= 400) {
            System.err.println("request failed with response code " + responseCode);
            String errorLine;
            final InputStream errorStream = connection.getErrorStream();
            if (errorStream != null) {
                BufferedReader errorReader = new BufferedReader(new InputStreamReader(errorStream));
                while ((errorLine = errorReader.readLine()) != null) {
                    System.err.println(errorLine);
                }
            }
            System.exit(3);
        }
        final InputStream inputStream = connection.getInputStream();
        if (inputStream == null) {
            return "";
        }
        BufferedReader responseReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = responseReader.readLine()) != null) {
            sb.append(line.trim());
        }
        return sb.toString();
    }

    private static void print(String message) {
        System.out.println(message);
    }

}
